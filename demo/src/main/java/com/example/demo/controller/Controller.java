package com.example.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/simulator")
public class Controller {

    @GetMapping("/test")
    public ResponseEntity<String> test() {
        LocalDate currentDate = LocalDate.now();
        return ResponseEntity.ok("Today's date is: " + currentDate.toString());
    }
}
