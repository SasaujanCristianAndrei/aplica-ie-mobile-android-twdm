import axios from 'axios';

export const getTimeInBucharest = async () => {
  try {
    const response = await axios.get('http://worldtimeapi.org/api/timezone/Europe/Bucharest');
    const data = response.data;
    return data;
  } catch (error) {
    console.error('Error:', error);
    throw error;
  }
};

export const getServerProrpiu = async () => {
  try {
    const response = await axios.get('http://172.20.10.2:8080/api/simulator/test');
    const data = response.data;
    return data;
  } catch (error) {
    console.error('Error:', error);
    throw error;
  }
};