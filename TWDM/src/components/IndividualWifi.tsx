import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';

const IndividualWifi = ({ wifiData }) => {
  const renderItem = ({ item }) => (
    <View style={styles.item}>
      <Text>BSSID: {item.BSSID}</Text>
      <Text>SSID: {item.SSID}</Text>
      <Text>Capabilities: {item.capabilities}</Text>
      <Text>Frequency: {item.frequency}</Text>
      <Text>Level: {item.level}</Text>
      <Text>Timestamp: {item.timestamp}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={wifiData}
        renderItem={renderItem}
        keyExtractor={(item) => item.BSSID} // Use BSSID as the unique key
      />
    </View>
  );
};

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
});

export default IndividualWifi;
