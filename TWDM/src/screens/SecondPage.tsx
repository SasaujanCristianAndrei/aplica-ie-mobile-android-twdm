import React from 'react';
import { View } from 'react-native';
import { WifiEntry } from 'react-native-wifi-reborn';
import IndividualWifi from '../components/IndividualWifi';

interface Props {
    route: {
        params: {
            wifiList: WifiEntry[];
        }
    }
}

const SecondPage: React.FC<Props> = ({route: {params: {wifiList}}}) => {
  return (
    <View style={{ flex: 1 }}>
      <IndividualWifi wifiData={wifiList} />
    </View>
  );
};

export default SecondPage;
