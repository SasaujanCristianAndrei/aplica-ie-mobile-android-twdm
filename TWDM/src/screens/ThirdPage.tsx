import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, Modal, Alert, Pressable } from "react-native";
import NetInfo from '@react-native-community/netinfo';
import { useNavigation } from "@react-navigation/native";


interface NetworkDetails {
  ssid: string | null;
  bssid: string | null;
  strength: number | null;
  ipAddress: string | null;
  subnet: string | null;
  frequency: number | null;
  linkSpeed: number | null;
  rxLinkSpeed: number | null;
  txLinkSpeed: number | null;
}

const ThirdPage = () => {
  const navigation = useNavigation();
  const [networkDetails, setNetworkDetails] = useState<NetworkDetails | null>(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');


  useEffect(() => {
    const fetchNetworkDetails = async () => {
      try {
        const networkState = await NetInfo.fetch();
        if (networkState.isConnected) {
          setNetworkDetails(networkState.details);
        } else {
          setErrorMessage('No Wi-Fi networks detected');
          setModalVisible(true);
        }
      } catch (error) {
        setErrorMessage('Error loading network details: ' + error.message);
        setModalVisible(true);
      }
    };

    fetchNetworkDetails();
  }, []);

  return (
    <View style={styles.container}>
      {networkDetails && (
        <View>
          {/* Display network details */}
          <Text>SSID: {networkDetails.ssid}</Text>
          <Text>BSSID: {networkDetails.bssid}</Text>
          <Text>Signal Strength: {networkDetails.strength}</Text>
          <Text>IP Address: {networkDetails.ipAddress}</Text>
          <Text>Subnet: {networkDetails.subnet}</Text>
          <Text>Frequency: {networkDetails.frequency}</Text>
          <Text>Link Speed: {networkDetails.linkSpeed}</Text>
        </View>
      )}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(false);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{errorMessage}</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => {
                setModalVisible(false);
                navigation.navigate("Home" as never, {})
              }}>
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  button: {
    backgroundColor: 'blue',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default ThirdPage;