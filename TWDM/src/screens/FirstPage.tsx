import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  Modal,
  Alert,
  Pressable,
} from 'react-native';
import WifiManager from 'react-native-wifi-reborn';
import {getServerProrpiu, getTimeInBucharest} from '../services/ApiService';

const FirstPage = () => {
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [wifiList, setWifiList] = useState<any[]>([]);
  const handleLoadWifiList = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location permission is required for WiFi connections',
          message:
            'This app needs location permission as this is required to scan for wifi networks.',
          buttonNegative: 'DENY',
          buttonPositive: 'ALLOW',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        const list = await WifiManager.loadWifiList();
        setWifiList(list);
        navigation.navigate('Wifi-List' as never, {wifiList: list});
      } else {
        setErrorMessage('Location permission denied');
        setModalVisible(true);
      }
    } catch (error) {
      setErrorMessage('Error loading wifi list: ' + error.message);
      setModalVisible(true);
    }
  };

  const makeApiCallExtern = async () => {
    try {
      const data = await getTimeInBucharest();
      Alert.alert('Data:', JSON.stringify(data));
    } catch (error) {
      console.error('Error:', error);
      Alert.alert('Error', 'Failed to fetch data from the server');
    }
  };

  const makeApiCallPropriu = async () => {
    try {
      const data = await getServerProrpiu();
      Alert.alert('Data:', JSON.stringify(data));
    } catch (error) {
      console.error('Error:', error);
      Alert.alert('Error', 'Failed to fetch data from the server');
    }
  };

  const handleShowIp = async () => {
    navigation.navigate('Show-Ip' as never, {});
  };

  return (
    <SafeAreaView style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{errorMessage}</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <StatusBar barStyle="dark-content" />
      <View style={styles.content}>
        <TouchableOpacity style={styles.button} onPress={handleLoadWifiList}>
          <Text style={styles.buttonText}>Load WiFi List</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, {marginTop: 10}]}
          onPress={handleShowIp}>
          <Text style={styles.buttonText}>Show Ip</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, {marginTop: 10}]}
          onPress={makeApiCallPropriu}>
          <Text style={styles.buttonText}>Comunicare telefon server propriu</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, {marginTop: 10}]}
          onPress={makeApiCallExtern}>
          <Text style={styles.buttonText}>Comunicare telefon server extern</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  button: {
    backgroundColor: 'blue',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default FirstPage;
