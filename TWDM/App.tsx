import React, {useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import FirstPage from './src/screens/FirstPage';
import SecondPage from './src/screens/SecondPage';
import ThirdPage from './src/screens/ThirdPage';
import {createNativeStackNavigator} from '@react-navigation/native-stack';


const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={FirstPage}
          options={{title: 'Home'}}
        />
        <Stack.Screen 
        name="Wifi-List" 
        component={SecondPage}
        options={{title: 'Available WiFi List'}} 
        />
        <Stack.Screen 
        name="Show-Ip" 
        component={ThirdPage}
        options={{title: 'Show ip'}} 
        />
      </Stack.Navigator>
    </NavigationContainer>
  );

  };
export default App;
